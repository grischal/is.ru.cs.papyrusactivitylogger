package is.ru.cs.papyrusActivityLogger;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.EStructuralFeatureImpl;
import org.eclipse.emf.transaction.NotificationFilter;
import org.eclipse.emf.transaction.ResourceSetChangeEvent;
import org.eclipse.emf.transaction.ResourceSetListener;
import org.eclipse.emf.transaction.RollbackException;
import org.eclipse.papyrus.infra.core.sashwindows.di.PageRef;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//TODO: When the user closes the model we get no indication of whether he saved or not
/**
 * Listener to log changes in an EMF model.
 * Contains methods for specialized logging for Papyrus but works
 * in general for other models as well.
 * @author Alexander
 *
 */
public class PapyrusModelModificationListener implements ResourceSetListener {
	protected long timeSinceLastChange = 0;
//	public ArrayList<String> loggerlist = new ArrayList<String>();
	protected ArrayList<String> loggerListTime = new ArrayList<String>();
//	public ArrayList<String> loggerInfo = new ArrayList<String>();
	protected String[] ignored = {"org.eclipse.gmf.runtime.notation.impl", 
		"org.eclipse.papyrus.infra.gmfdiag.css",
		"org.eclipse.uml2.uml.internal.resource"};
//	protected String[] blackListed = {};
	
	private LoggingFormatter loggingFormatter;
	private UserAction userAction;
	
	protected static final Logger LOG = LoggerFactory.getLogger(PapyrusModelModificationListener.class);
	
	//Keeping track of all user actions over time
	protected List<UserAction> userActions;
	
	/**
	 * Indicates in the logs the this listener was started.
	 */
	public PapyrusModelModificationListener () {
		LOG.debug("Logging STARTED in " + this.getClass().toString());
		//Initiates the time to 0 so that the first action is correctly recognised.
		this.timeSinceLastChange = 0;
		this.loggingFormatter = new DefaultLoggingFormatter();
		
		userActions = new LinkedList<UserAction>();
		
	}

	// Easy way of allowing the user to give us the formatter (could also use some set method)
	/**
	 * Intended to be used when you want to specify which formatter you want to use for the logging.
	 * Currently not used.
	 * @param formatter  the formatter to use for the printing
	 */
	public PapyrusModelModificationListener (LoggingFormatter formatter) {
		
		//Initiates the time to 0 so that the first action is correctly recognised.
		this.timeSinceLastChange = 0;
		this.loggingFormatter = formatter;
		
	}

	@Override
	public NotificationFilter getFilter() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAggregatePrecommitListener() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isPostcommitOnly() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isPrecommitOnly() {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * Used to change which features should be ignored.
	 * @param ignored new list of features to be ignored.
	 */

	public void setIgnored(String[] ignored) {
		this.ignored = ignored;
	}
	
	public void getTime() {
		Date currentDate = new Date();
		SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
		this.loggerListTime.add(timeFormat.format(currentDate));
		return;
	}
	
	/**
	 * Used to a list of all user actions since this logger started.
	 * @return  the list of user actions.
	 */
	public List<UserAction> getUserActions() {
		return this.userActions;
	}
		
	/**
	 * Called every time the ResourceSet changes. This means both changes in the diagram editor, and in the model editor view.
	 */
	@Override
	
	public void resourceSetChanged(ResourceSetChangeEvent arg0) {
		
		//Calculate how much time passed since the last change.
		//We will be using this later to determine whether this is a follow-up change to a previous change, or if it's actually a user action
		
		long timeDelta = System.currentTimeMillis() - this.timeSinceLastChange;

		//Every "Human action" consists of a number of calls to resourceSetChanged, each containing 1..* notifications.
		//The task is to classify what human action has taken place.
		
		boolean somethingPrinted = false;

		for (Notification currentNot : arg0.getNotifications()) {	
			Object feature = currentNot.getFeature();
			Object oldValue = currentNot.getOldValue();
			Object newValue = currentNot.getNewValue();
			Object notifier = currentNot.getNotifier();
			
			//Skip processing of ignored elements.
			if (isIgnoredFeature(feature) || isIgnoredValue(oldValue) 
					|| isIgnoredValue(newValue) || isIgnoredValue(notifier)) {
				continue;
			}
			
			somethingPrinted = true;
			
			// We have a new user action so we print New Command
			if (timeDelta > 200) {
//				//TODO: Skipping this for now.
//				if (userAction != null) {					
//					//We are finished adding to the old action so we send it to the logging formatter and log it
//					String tolog = this.loggingFormatter.getStringToLog(userAction);
//					
//					LOG.info(tolog);
//				}
				
				userAction = new UserAction();
				
				//Due to call by reference, the values will be updated in the list as well if we change later. 
				userActions.add(userAction);

				LOG.info("New User Action");
				
				//Set the timeDelta to 0 so we don't print new command multiple times for the same action
				timeDelta = 0;
			}
			
			String methodString;
			String featureString;
			String oldValueString;
			String newValueString;
			String pathString;

			switch(currentNot.getEventType()) {
			case Notification.ADD:
				methodString = "ADD";
				break;
			case Notification.SET:
				methodString = "SET";
				break;
			case Notification.REMOVE:
				methodString = "REMOVE";
				break;
			case Notification.REMOVE_MANY:
				methodString = "REMOVE MANY";
				break;
			case Notification.ADD_MANY:
				methodString = "ADD MANY";
				break;
			default:
				methodString = "OTHER: " + currentNot.getEventType();
			}
			
			
			//We check if it is a EStructuralFeatureImpl in the isIgnoredFeature method (for now)
			featureString = ((EStructuralFeatureImpl) feature).getName();
			
			//Print out what oldValue belongs to until we get to the notifier
			oldValueString = printValue(oldValue, notifier);

			//Print out what newValue belongs to until we get to the notifier
			newValueString = printValue(newValue, notifier);
			
			pathString = printPath(notifier, null);
			
			LoggingNotification not = new LoggingNotification(
					methodString, 
					featureString, 
					oldValueString, 
					newValueString, 
					pathString);
			
			LOG.info(not.toString());
			
			userAction.addNotification(not);
		}

		// This is checked to make sure we don't update the time if nothing was printed
		if (somethingPrinted) {			
			// Update the change time
			this.timeSinceLastChange = System.currentTimeMillis();
		}
	}

	// TODO: it would probably be better to merge the ignoredFeatures and ignoredValues into one function later
	// Decides whether the feature is ignored (meaning it does not need to be processed)
	/**
	 * Used to determine whether a feature type is ignored, meaning it should not be logged.
	 * @param feature  Object, the feature of a notification.
	 * @return  boolean, true if feature is ignored else false.
	 */
	private boolean isIgnoredFeature(Object feature) {
		if (!(feature instanceof EStructuralFeatureImpl)) {
			return true;
		}
		return false;
	}
	
	//Decides whether the value is ignored (meaning it does not need to be processed)
	/**
	 * Used to determine whether a value type is ignored, meaning it should not be logged.
	 * @param value  Object, the oldValue or newValue of a notification.
	 * @return  boolean, true if the value is ignored else false.
	 */
	private boolean isIgnoredValue(Object value) {
		if (value != null) {
			String s = value.getClass().getName();
			for (String ignored: this.ignored) {
				if (s.contains(ignored)) {
					return true;
				}
			}
		}
		return false;
	}
	
	//TODO: Handle GeneralizationImpl better
	// It might be possible to have the user give us the printElement functions so he can decide how he wants
	// to format the logging of specific elements
	/**
	 * Used to get information about the owner of the currentObject.
	 * Calls a recursive method that ends when the currentObject is null or not an instance of Element or 
	 * when it is the same as the notifier.
	 * @param currentObject  Object, the object to get the owners of.
	 * @param notifier  Object, the notifier in the original notification.
	 * @return
	 */
	private String printPath(Object currentObject, Object notifier) {
		return printPath(currentObject, notifier, new StringBuilder());
	}
	
	private String printPath(Object currentObject, Object notifier, StringBuilder sb) {
		if (currentObject != null && currentObject == notifier) return sb.toString();
		
		sb.append("(");
		
		if (currentObject == null) {
			sb.append("null, null)");
			return sb.toString();
		}

		else if (currentObject instanceof List<?>) {
			sb.append(printElement((List<?>) currentObject));
		}
		else if (currentObject instanceof Parameter) {
			sb.append(printElement((Parameter) currentObject));
		}
		else if (currentObject instanceof Property){
			sb.append(printElement((Property) currentObject));
		}
		else if (currentObject instanceof NamedElement) {
			sb.append(printElement((NamedElement) currentObject));
		}
		else if (currentObject instanceof PageRef) {
			sb.append(printElement((PageRef) currentObject));
		}
		else {
			sb.append(currentObject.toString());
		}
		
		sb.append(", " + currentObject.getClass().getSimpleName() + ")");
		
		if (currentObject instanceof Element) {
			return printPath(((Element) currentObject).getOwner(), notifier, sb);
		}
		return sb.toString();
	}
	
	// Simply calls printPath on the value
	/**
	 * Calls printPath on the value
	 * @param value  Object, the oldValue or newValue of the notification.
	 * @param notifier  Object, the notifier of the notification.
	 * @return  Information about the owner of the value.
	 */
	private String printValue(Object value, Object notifier) {
		return printPath(value, notifier);
	}

	private String printElement(Parameter parameter) {
		StringBuilder str = new StringBuilder();
		str.append("(" + parameter.getName());
		
		Type type = parameter.getType();
		if (type != null) {
			str.append(", " + type.getName());
		}
		else {
			str.append(", undefined");
		}
		
		ParameterDirectionKind direction = parameter.getDirection();
		if (direction != null) {
			str.append(", " + direction.getName());
		}
		str.append(") ");
		return str.toString();
	}
	
	// list print
	private String printElement(List<?> elementList) {
		StringBuilder str = new StringBuilder();
		str.append("[");
		for (Object element: elementList) {
			if (element instanceof Parameter) {
				str.append(printElement((Parameter) element));
			}
			else {
				str.append(printElement(element));
			}
		}
		str.append("]");
		return str.toString();
	}
	
	// named element print
	private String printElement(NamedElement element) {
		return "(" + element.getName() + ")";
	}
	
	// default print
	private String printElement(Object object) {
		if (object instanceof NamedElement) {
			return printElement((NamedElement) object);
		}
		else {
			return "(" + object + ")";
		}
	}

	// property print
	private String printElement(Property property) {
		StringBuilder str = new StringBuilder();
		str.append("(" + property.getName());

		Type type = property.getType();
		if (type != null) {
			str.append(", " + type.getName());
		}
		else {
			str.append(", undefined");
		}
		str.append(")");
		
		return str.toString();
	}
	
	// printing which view in the model is selected
	private String printElement(PageRef pageRef) {
		 EObject e = pageRef.getEmfPageIdentifier();
		if (e instanceof EObject) {
			EStructuralFeature name = e.eClass().getEStructuralFeature("name");
			if (name != null) {
				String nameString = (String) e.eGet(name);
				if (nameString != null) {
					return "(" + nameString + ")";
				}
			}
		}
		return "(" +  e.toString() + ")";
	}
	
	@Override
	public Command transactionAboutToCommit(ResourceSetChangeEvent arg0) throws RollbackException {
		// For now we are not doing anything here.
		return null;
	}
	
//	// Sets the time the listener is active (times is in minutes)
//	public void setTime(int time) {
//		this.maxTime = time*60*1000; // Converting minutes to milliseconds
//		this.startTime = System.currentTimeMillis();
//	}
//	
//	private boolean timesUp() {
//		if (this.maxTime != -1) {
//			return this.maxTime < (System.currentTimeMillis() - this.startTime);
//		}
//		return false;
//	}
//
//	public void setSurvey(String survey) {
//		//Setting the survey for the popup
//		this.survey = survey;
//	}
//	
//	private void doPopup() {
//		//Shows a popup asking the user to open the survey provided with he set survey method
//		//Only show the popup if the survey was provided
//		if (this.survey != null && this.survey != "") {
//			//Only show the popUp once
//			if (!this.popUpShown) {
//				NotificationPopUpUI popup = new NotificationPopUpUI(Display.getDefault());
//				popup.setSurveyLink(survey);
//				popup.setFadingEnabled(false);
//				popup.open();
//				this.popUpShown = true;
//			}
//		}
//	}
}

