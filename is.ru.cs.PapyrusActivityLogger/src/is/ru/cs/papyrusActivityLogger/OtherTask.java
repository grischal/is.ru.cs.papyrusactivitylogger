package is.ru.cs.papyrusActivityLogger;

import org.eclipse.emf.transaction.ResourceSetListener;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

public class OtherTask {
	ResourceSetListener listener;
	TransactionalEditingDomain attachedTo;
	long time;
	long startTime;
	protected boolean started;
	ListenerEndHandling end;
	
	public OtherTask(ResourceSetListener listener, TransactionalEditingDomain attachedTo, long time, ListenerEndHandling end) {
		attachedTo.addResourceSetListener(listener);
		this.listener = listener;
		this.attachedTo = attachedTo;
		this.time = time;
		this.started = false;
		this.end = end;
	}
	
	public boolean timesUp() {
		return this.started && (this.startTime - System.currentTimeMillis()) > this.time;
	}
	
	public void end() {
		this.end.performEnd();
	}
	
	public void start() {
		//TODO: make this open the task project
		this.startTime = System.currentTimeMillis();
		this.started = true;
	}
}
