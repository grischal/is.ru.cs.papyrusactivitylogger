package is.ru.cs.papyrusActivityLogger;

/**
 * The default logging format used by the PapyrusModelModificationListener.
 * @author Alexa
 *
 */
public class DefaultLoggingFormatter implements LoggingFormatter {
	
	/**
	 * Adds new lines between all notifications in the action.
	 */
	@Override
	public String getStringToLog(UserAction action) {
		if (action == null) return "";
		StringBuilder sb = new StringBuilder();
		sb.append(action.timestamp);
		for (LoggingNotification notification : action.notifications) {
			sb.append("\n" + notification.toString());
		}
		return sb.toString();
	}
}
