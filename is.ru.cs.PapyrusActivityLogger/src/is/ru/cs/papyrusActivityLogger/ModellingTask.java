package is.ru.cs.papyrusActivityLogger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

import is.ru.cs.papyrusActivityLogger.extensions.ModelCreator;
import is.ru.cs.papyrusActivityLogger.ui.TaskEndNotification;
import is.ru.cs.papyrusActivityLogger.ui.TaskView;

/**
 * Used to create and open models and adding a logger to it.
 * Controlled by the TaskManager.
 * Created in the ImportTaskMaker.
 * @author Alexander
 *
 */
public class ModellingTask extends Task{

	private long startTime;
	private List<String> modelURIs;
	private List<Model> models;
	private String modelType = "is.ru.cs.PapyrusActivityLogger.modelMaker.papyrus";
	private static IExtensionRegistry registry = Platform.getExtensionRegistry();
	
	/**
	 * Creates empty models if the filename in the modelURI is None
	 * @param name  name of the task
	 * @param time  time to keep the task active, -1 if this should be an untimed task
	 * @param description  description of the task
	 * @param modelURI  String, path to the model this task deals with.
	 */
	public ModellingTask(String name, long time, String description, String modelURI) {
		super(name, time, description);
		modelURIs = Arrays.asList(modelURI);
		models = new ArrayList<Model>();
	}
	
	/**
	 * Creates empty models for all models in modelURIs whose filename is None
	 * @param name  name of the task
	 * @param time  time to keep the task active, -1 if this should be an untimed task
	 * @param description  description of the task
	 * @param modelURIs  List containing the model URIs used in this task
	 */
	public ModellingTask(String name, long time, String description, List<String> modelURIs) {
		super(name, time, description);
		this.modelURIs = modelURIs;
		models = new ArrayList<Model>();
	}
	
	/**
	 * Checks the nextButtonPressed of the task view if this was an untimed task.
	 * Checks if the time is up if the was a timed task.
	 */
	@Override
	public boolean taskOver() {
		//Infinite time break
		if (this.time ==  -1) {
			//Check if the next button is pressed
			return started && this.taskView.nextButtonPressed();
		} else {			
			//Check if the time is over
			return started && (System.currentTimeMillis() - startTime) > time;			
		}
	}

	/**
	 * Hides all UI elements in the task view.
	 * Removes the listener from the model.
	 * Displays a pop-up indicating the task is over if this was a timed task.
	 */
	@Override
	public void end() {		
		taskView.hideAll();
		System.out.println("In model end");
		
		//Removing the listeners of all domains
		removeListeners();
		
		//Check that the time is not -1 (meaning the task ended because of a timeout not because of a next button press)
		if (this.time != -1) {
			//Displaying a popup indicating that the task is now over
			TaskEndNotification.openInformationBox("The modelling task has now ended. Please refer to the task view for instructions on a new task");
		}
	}

	/**
	 * Removes listeners from all models in this task
	 */
	private void removeListeners() {
		for (Model model : models) {
			model.removeListener();
		}
	}

	/**
	 * Calls updateTaskView method, opens the model and adds a listener to it.
	 * Sets the startTime field.
	 */
	@Override
	public void start(TaskView taskView) {
		System.out.println("In model start");
		startTime = System.currentTimeMillis();
		started = true;

		//Create the Model needed (also created model files if needed)
		createModels();
		//Open the models
		openModels();

		//Update the view information
		updateTaskView(taskView);

	}

	/**
	 * Reveals the name and description labels. Reveals the next button if this is an untimed modelling task.
	 * Updates the taskView field.
	 */
	@Override
	public void updateTaskView(TaskView taskView) {
		//Reveal the task name label
		taskView.updateName(name);
		//Reveal the task description label
		taskView.updateDescription(description);
		
		//Time is -1 indicates that this is an infinite time task
		if (this.time ==  -1) {
			//Add a button to the task view that takes us to the next task
			//Don't require a some URL to be pressed to allow the button press
			taskView.updateNextButton(false);
		}
		
		//Adding buttons to allow the users to reopen the models if he closes them
		taskView.updateModelOpeningButton(models);
		
		//Updating the task taskView
		this.taskView = taskView;
	}
	
	/**
	 * Opens all models in this task.
	 * Adds a PapyrusModelModificationListener to the model
	 */
	private void openModels() {
		for (Model model : models) {
			try {
				model.open();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Creates the Models and also created empty model files when needed
	 * @throws CoreException 
	 */
	private void createModels() {
		for (String modelURI : modelURIs) {
			File file = new File(modelURI);
			if (file.getName().equals("None")) {
				String directory = modelURI.substring(0,modelURI.length()-4);
				IConfigurationElement[] config = registry.getConfigurationElementsFor("is.ru.cs.papyrusActivityLogger.model.creation");
				int i = 0;
				while (i < config.length) {
					IConfigurationElement e = config[i];
					if (e.getAttribute("id").equals(this.modelType)) {
						Object creator;
						try {
							creator = e.createExecutableExtension("class");
							if (creator instanceof ModelCreator) {
								modelURI = ((ModelCreator) creator).createModel(directory, name);
							}
						} catch (CoreException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					i++;
				}
				
//					modelURI = createEmptyPapyrusModel(directory, name, 0);
			}
			models.add(new Model(modelURI));
		}
	}
}
