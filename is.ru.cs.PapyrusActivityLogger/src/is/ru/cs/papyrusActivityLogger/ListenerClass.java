package is.ru.cs.papyrusActivityLogger;

import org.eclipse.emf.transaction.ResourceSetListener;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

public class ListenerClass {

	private ResourceSetListener listener;
	private TransactionalEditingDomain attachedTo;
	private long time;
	private ListenerEndHandling end;
	private long startTime;


	
	public ListenerClass(ResourceSetListener listener, TransactionalEditingDomain attachedTo, long time, ListenerEndHandling end) {
		attachedTo.addResourceSetListener(listener);
		this.listener = listener;
		this.attachedTo = attachedTo;
		this.time = time;
		this.startTime = System.currentTimeMillis();
		this.end = end;
	}
	
	public boolean timesUp() {
		return (System.currentTimeMillis()-this.startTime) > this.time;		
	}
	
	public void end() {
		this.attachedTo.removeResourceSetListener(listener);
		this.end.performEnd();
	}
}
