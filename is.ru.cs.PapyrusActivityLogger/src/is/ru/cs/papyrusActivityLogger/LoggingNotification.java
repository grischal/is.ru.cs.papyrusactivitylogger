package is.ru.cs.papyrusActivityLogger;


/**
 * Contains information about a PapyrusModelModificationListener resourceSetChanged function call.
 * Has the method type, feature type, old value of the field being changed, new value of the field being changed and 
 * information about the owners of the field being changed.
 * @author Alexander
 *
 */
public class LoggingNotification {
	//Maybe smarter to not store the strings but the actual objects instead
	public String method;
	public String feature;
	public String oldValue;
	public String newValue;
	public String objectPath;

	/**
	 * 
	 * @param method  The method type
	 * @param feature  The feature type
	 * @param oldValue  The old value
	 * @param newValue  The new value
	 * @param objectPath  Information about the owners of the field
	 */
	public LoggingNotification (String method, String feature, String oldValue, String newValue, String objectPath) {
		this.method = method;
		this.feature = feature;
		this.oldValue = oldValue;
		this.newValue = newValue;
		this.objectPath = objectPath;
	}
	
	/**
	 * Returns all fields separated by a tab
	 */
	public String toString() {
		return this.method + "	" + this.feature + "	" + this.oldValue + "	" + this.newValue + "	" + this.objectPath;
	}
}
