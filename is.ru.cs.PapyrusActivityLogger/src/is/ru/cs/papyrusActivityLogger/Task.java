package is.ru.cs.papyrusActivityLogger;

import is.ru.cs.papyrusActivityLogger.ui.TaskView;

/**
 * Should be extended to configure the functionality of the task type.
 * Contains various methods called by the TaskManager.
 * @author Alexander
 *
 */
public abstract class Task {
	protected String name;
	protected long time;
	protected String description;
	protected boolean taskOver;
	protected TaskView taskView;
	protected boolean started;
	
	/**
	 * Stores the given parameters in the task instance. Sets the taskOver and started fields to false.
	 * @param name  String, name of the task
	 * @param time  long, duration, in minutes, to keep the task active. In case of an untimed task set this as -1.
	 * @param description  String, description of the task
	 */
	public Task(String name, long time, String description) {
		this.name = name;
		if (time != -1) {
			if (Activator.DEBUG) {
				//Shorten time by factor 100 to do "quick" debugging.
				this.time = time*600;
			} else {
				//Time comes in minutes, so multiply by 60*1000 to obtain ms.
				this.time = time*60*1000;
			}
		}
		else {
			this.time = time;
		}
		this.description = description;
		this.taskOver = false;
		this.started = false;
	}
	
	/**
	 * This method is called when this task becomes active in the TaskManager.
	 * @param taskView  TaskView, the currently active task view
	 */
	public abstract void start(TaskView taskView);
	/**
	 * Called in the TaskManager to check if the task is over
	 * @return  boolean, true if the task is over else false.
	 */
	public abstract boolean taskOver();
	/**
	 * Called in the TaskManager when this task is over.
	 */
	public abstract void end();
	/**
	 * This method should call update methods of the active TaskView when the task is started or when the active TaskView changes.
	 * Called from TaskManager when a new TaskView instance is opened.
	 * @param taskView  TaskView, the currently active task view.
	 */
	public abstract void updateTaskView(TaskView taskView);
}
