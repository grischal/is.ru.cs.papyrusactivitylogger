package is.ru.cs.papyrusActivityLogger;

import java.util.ArrayList;
import java.util.List;

import is.ru.cs.papyrusActivityLogger.ui.TaskView;

/**
 * Used for giving the user links to open.
 * Controlled by the TaskManager.
 * Created in the ImportTaskMaker.
 * @author Alexander
 *
 */
public class URLTask extends Task{
	private List<String> urls;
	
	/**
	 * Used for URL tasks containing only one URL.
	 * @param name  name of the task.
	 * @param time  time of task, should be -1 since this is an untimed task.
	 * @param description  description of the task.
	 * @param url  the URL to be opened.
	 */
	public URLTask(String name, long time, String description, String url) {
		super(name, time, description);
		this.urls = new ArrayList<String>();
		this.urls.add(url);
	}

	/**
	 * Used for URL tasks containing multiple URLs
	 * @param name  name of the task
	 * @param time  should be -1 since this is an untimed task
	 * @param description  description of the task
	 * @param urls  list of the URLs in this task.
	 */
	public URLTask(String name, long time, String description, List<String> urls) {
		super(name, time, description);
		this.urls = urls;
	}	
	
	/**
	 * Calls the nextButtonPressed of the taskView to determine if the task is over.
	 */
	@Override
	public boolean taskOver() {
		return started && taskView.nextButtonPressed();
	}

	/**
	 * Hides all UI elements in the taskView
	 */
	@Override
	public void end() {
		taskView.hideAll();
		System.out.println("In ui end");
	}
	
	/**
	 * Called to start the task.
	 * Calls the updateTaskView method.
	 */
	@Override
	public void start(TaskView taskView) {
		started = true;
		updateTaskView(taskView);
		System.out.println("In ui start");
	}

	/**
	 * Reveals name and description labels, link and a grayed out next button.
	 * Updates the taskView field.
	 */
	@Override
	public void updateTaskView(TaskView taskView) {
		//Revealing and updating name, description and link information
		taskView.updateName(name);
		taskView.updateDescription(description);
		taskView.updateLink(urls);
		//Require the button to be grayed out until the url is clicked
		taskView.updateNextButton(true);
		
		//Updating the task taskView
		this.taskView = taskView;
	}

}
