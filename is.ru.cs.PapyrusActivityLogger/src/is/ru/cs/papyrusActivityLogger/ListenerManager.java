package is.ru.cs.papyrusActivityLogger;

import java.util.ArrayList;
import java.util.Timer;

public class ListenerManager {

	protected static ArrayList<ListenerClass> listeners = new ArrayList<ListenerClass>();
	private static boolean timerStarted = false;
	
	public static void addListener(ListenerClass listener) {
		if (!timerStarted) {
			Timer timer = new Timer();
			timer.schedule(new CheckListeners(), 10000, 5000);
			timerStarted = true;
		}
		listeners.add(listener);
	}
	
	//TODO: You might want to check the Eclipse Jobs API for handling the timers!
	//https://www.eclipse.org/articles/Article-Concurrency/jobs-api.html
	static void checkListeners() {
		ArrayList<ListenerClass> toRemove = new ArrayList<ListenerClass>();
		for (ListenerClass lc : listeners) {
			if (lc.timesUp()) {
				toRemove.add(lc);
			}
		}
		for (ListenerClass lc : toRemove) {
			lc.end();
			listeners.remove(lc);
		}
	}
	
}
