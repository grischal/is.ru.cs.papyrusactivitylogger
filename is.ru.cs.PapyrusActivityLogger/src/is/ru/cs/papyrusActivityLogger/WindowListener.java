package is.ru.cs.papyrusActivityLogger;

import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;

//TODO: Add a logging component to this

public class WindowListener implements IWindowListener {

	@Override
	public void windowActivated(IWorkbenchWindow window) {
//		System.out.println("Activated " + window.toString() + " "+ window.getActivePage().toString() + " " + window.getActivePage().getLabel());
		IWorkbenchPage wap = window.getActivePage();
		IEditorPart aped = wap.getActiveEditor();
		String s = aped.getTitle();
		System.out.println("Activated " + s);
	}

	@Override
	public void windowDeactivated(IWorkbenchWindow window) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(IWorkbenchWindow window) {
		IWorkbenchPage wap = window.getActivePage();
		IEditorPart aped = wap.getActiveEditor();
		String s = aped.getTitle();
		System.out.println("Closed " + s);
	}

	@Override
	public void windowOpened(IWorkbenchWindow window) {
		IWorkbenchPage wap = window.getActivePage();
		IEditorPart aped = wap.getActiveEditor();
		String s = aped.getTitle();
		System.out.println("Opened " + s);
	}

}
