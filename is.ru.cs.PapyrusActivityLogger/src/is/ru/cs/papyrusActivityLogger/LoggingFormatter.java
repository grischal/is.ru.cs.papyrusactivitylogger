package is.ru.cs.papyrusActivityLogger;

/**
 * Should be implemented to customize logging output.
 * @author Alexander
 *
 */
public interface LoggingFormatter {

	public String getStringToLog(UserAction action);

}
