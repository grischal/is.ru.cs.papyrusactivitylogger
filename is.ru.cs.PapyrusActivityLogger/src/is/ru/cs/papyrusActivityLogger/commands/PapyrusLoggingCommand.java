package is.ru.cs.papyrusActivityLogger.commands;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.Bundle;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import is.ru.cs.papyrusActivityLogger.PapyrusModelModificationListener;

/**
 * This command is added as an icon to the toolbar. Its purpose is to manually turn on the logging.
 * @author grischal
 *
 */
public class PapyrusLoggingCommand extends AbstractHandler {

	protected PapyrusModelModificationListener modelListener = null;

	protected ModelSet modelSet;
	
	private List<TransactionalEditingDomain> domainList = new ArrayList<TransactionalEditingDomain>();
	
//	private long startTime = -1;
	
	/**
	 * This method is called when the command is executed (= when the icon is clicked in the toolbar)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		configureLogbackInBundle(Platform.getBundle("is.ru.cs.papyrusActivityLogger"));
		//Init the listener
		this.modelListener = new PapyrusModelModificationListener();
		
		// Attach the listener to the papyrus model ResourceSet if there isn't already a listener on it
		// (Need to check how this works when creating a LoggingTask using the wizard)
		TransactionalEditingDomain domain = getDomain();
		if (!domainList.contains(domain)) {
			domain.addResourceSetListener(modelListener);
			domainList.add(domain);
		}
		
		return null;
	}
	
	/**
	 * Copied from org.eclipse.papyrus.revision.tool.core.ReviewResourceManager
	 * Gets the active editor and returns it
	 * @return
	 */
	protected IWorkbenchPart getBootstrapPart() {
		IWorkbenchPart part = null;
		try {
			IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			if (activeWorkbenchWindow != null) {
				IWorkbenchPage activePage = activeWorkbenchWindow.getActivePage();
				if (activePage != null) {
					part = activePage.getActiveEditor();
				}
			}
		} catch (NullPointerException e) {
			// An element is not active yet
			return null;
		}
		
		//Does the editor indeed have the right class (=is it a diagram editor in Papyrus)?
		if (part instanceof IMultiDiagramEditor) {
			return part;
		}
		
		// The current active part is not for us.
		return null;
	}

	/**
	 * Copied from org.eclipse.papyrus.revision.tool.core.ReviewResourceManager
	 * 
	 * Returns the editing domain on top of the model ResourceSet
	 * 
	 * @return the editing domain of papyrus
	 */
	public TransactionalEditingDomain getDomain() {
		return getCurrentModelSet().getTransactionalEditingDomain();
	}
	
	/**
	 * 
	 * Copied from org.eclipse.papyrus.revision.tool.core.ReviewResourceManager
	 * 
	 * Returns the model ResourceSet
	 * 
	 * @return the current model set of papyrus.
	 */
	public ModelSet getCurrentModelSet(){
		// Commenting this out seemed to fix the issue of not being able to log multiple models at the same time
		// I will leave it in for now if it did something other than making sure we don't add multiple listeners
//		if(modelSet!=null){
//			return modelSet;
//		}
		IWorkbenchPart part=getBootstrapPart();
		if (part instanceof IMultiDiagramEditor){
			ServicesRegistry registry=((IMultiDiagramEditor)part).getServicesRegistry();
			try {
				modelSet = registry.getService(ModelSet.class);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		return modelSet;
	}

	/**
	 * Configures the Logback/SLF4J logger using the logback.xml config file located in the bundle.
	 * 
	 * @param bundle The plugin bundle containing the xml config
	 */
	private void configureLogbackInBundle(Bundle bundle) {
		try {
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        JoranConfigurator jc = new JoranConfigurator();
        jc.setContext(context);
        context.reset();
        
        //Set filename for logging to file.
        //TODO: For testing purposes, we write into a predefined filename in the bundle
        context.putProperty("file-name", Platform.getStateLocation(bundle).append("papyrusTestLog.log").toString());

        URL logbackConfigFileUrl = FileLocator.find(bundle, new Path("logback.xml"),null);
        jc.doConfigure(logbackConfigFileUrl.openStream());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JoranException e) {
			e.printStackTrace();
		}
    }
}
