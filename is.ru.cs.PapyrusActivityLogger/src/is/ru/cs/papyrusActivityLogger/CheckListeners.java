package is.ru.cs.papyrusActivityLogger;

import java.util.TimerTask;

public class CheckListeners extends TimerTask {

	@Override
	public void run() {
		System.out.println("Listeners checked");
		ListenerManager.checkListeners();		
	}

}
