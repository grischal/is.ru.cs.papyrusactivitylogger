package is.ru.cs.papyrusActivityLogger;

import java.util.TimerTask;

/**
 * Used for scheduling TaskManager.checkTasks() method calls.
 * @author Alexander
 *
 */
public class CheckTasks extends TimerTask {

	@Override
	public void run() {
//		System.out.println("Tasks checked");
		TaskManager.checkTasks();
	}

}
