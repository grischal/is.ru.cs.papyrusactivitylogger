package is.ru.cs.papyrusActivityLogger.ui;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * 
 * @author Alexander
 *
 */
public class PageOne extends WizardPage {
	private Text text1;
	private String filename;
	private Composite container;
	private Label label2;
	private Label label3;
	
	public PageOne() {
		super("First Page");
		setTitle("First Page");
		setDescription("Task Wizard: Fist Page");
	}
		
	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.None);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;
		
		createTimeField(container);
		createFileSelection(container);
		
		setControl(container);
		setPageComplete(false);
	}
	
	private void createTimeField(Composite parent) {
		// Time selection field
		Label label1 = new Label(parent, SWT.None);
		label1.setText("Enter time (min): ");		
		text1 = new Text(parent, SWT.BORDER | SWT.SINGLE);
		text1.setText("");
		text1.addKeyListener(new KeyListener() {
			
			@Override
			public void keyPressed(KeyEvent e) {
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				String text = text1.getText();
				if (!text.isEmpty()) {
					try {
						int value = Integer.parseInt(text);
						if (1 <= value && value < 600) {
							setPageComplete(true);
						}
						else {
							setPageComplete(false);
						}
					} catch (NumberFormatException ex) {
						setPageComplete(false);
					}
				}
				else {
					setPageComplete(false);
				}
			}
		});
		
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		text1.setLayoutData(gd);
	}

	//TODO: Allow user to unselect
	//TODO: Allow user to 
	private void createFileSelection(Composite parent) {
		// File selection button
		label2 = new Label(container, SWT.None);
		label2.setText("Select file to log into (Optional): ");
		Button fileSelect = new Button(container, SWT.BUTTON1);
		fileSelect.setText("Browse");
		fileSelect.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub
				FileDialog dialog = new FileDialog(getShell(), SWT.OPEN);
				dialog.setFilterExtensions(new String [] {"*.txt"});
//						dialog.setFilterPath();
				String result = dialog.open();
				System.out.println(result);
				filename = result;
				label3.setText(result);
			}
		});
		//TODO: make this not ugly
		label3 = new Label(container,SWT.None);
		label3.setText("                                                                                                                                                                        ");
	}
	
	public String getText1() {
		return text1.getText();
	}
	
	public String getFileName() {
		return filename;
	}
}
