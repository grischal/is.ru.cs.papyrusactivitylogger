package is.ru.cs.papyrusActivityLogger.ui;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class NewPageOne extends WizardPage {
	
	private Composite container;
	private Text time;
	private int minTime = 1;
	private int maxTime = 600;
	private Label filename;
	private Text survey;
	private Text name;
	private Label directory;
	private boolean[] finished = {false,false,false};

	protected NewPageOne() {
		super("First Page");
	}
	
	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.None);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;
		
		addModel(container);

		setControl(container);
		setPageComplete(false);
		
	}

	private void addModel(Composite parent) {
		createNameField(parent);
		createDirectorySelection(parent);
		createTimeField(parent);
		createFileSelection(parent);
		createSurveyField(parent);
		
	}
	
	private void createNameField(Composite parent) {
		Label label1 = new Label(parent, SWT.None);
		label1.setText("Enter name: ");
		name = new Text(parent, SWT.BORDER | SWT.SINGLE);
		name.setText("");
		name.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO
				finished[0] = !name.getText().isEmpty();
				pageComplete();
			}
			
		});
	}
	
	private void createDirectorySelection(Composite parent) {
		// File selection button
		Label label = new Label(container, SWT.None);
		label.setText("Select directory save task: ");
		
		// This is done to skip this space (very dirty and could probably be solved in a better way)
		new Label(parent, SWT.None);
		
		Button fileSelect = new Button(container, SWT.BUTTON1);
		fileSelect.setText("Browse");
		fileSelect.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseUp(MouseEvent e) {
				// Opens a standard file selection window and allows the user to select *.di files
				DirectoryDialog dialog = new DirectoryDialog(getShell(), SWT.OPEN);
				
				String result = dialog.open();
				System.out.println(result);
				directory.setText(result);
				
				finished[1] = !result.isEmpty();
				pageComplete();
			}
		});
		
		//TODO: make this not ugly
		directory = new Label(container,SWT.BORDER | SWT.SINGLE);
		directory.setText("                                                                                                                                                                        ");
	}
	
	private void createTimeField(Composite parent) {
		Label label1 = new Label(parent, SWT.None);
		label1.setText("Enter time: ");
		time = new Text(parent, SWT.BORDER | SWT.SINGLE);
		time.setText("");
		time.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				String text = time.getText();
				if (!text.isEmpty()) {
					try {
						int value = Integer.parseInt(text);
						if (minTime <= value && value <= maxTime) {
							finished[2] = true;
							pageComplete();
							return;
						}
					} catch (NumberFormatException ex) {
						
					}
				}
				finished[2] = false;
				pageComplete();
			}
			
		});
		
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		time.setLayoutData(gd);
	}
	
	private void createFileSelection(Composite parent) {
		// File selection button
		Label label = new Label(container, SWT.None);
		label.setText("Select model/diagram to log from (optional): ");
		
		// This is done to skip this space (very dirty and could probably be solved in a better way)
		new Label(parent, SWT.None);
		
		Button fileSelect = new Button(container, SWT.BUTTON1);
		fileSelect.setText("Browse");
		fileSelect.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseUp(MouseEvent e) {
				// Opens a standard file selection window and allows the user to select *.di files
				FileDialog dialog = new FileDialog(getShell(), SWT.OPEN);
				dialog.setFilterExtensions(new String [] {"*.di"});
//						dialog.setFilterPath();
				
				String result = dialog.open();
				System.out.println(result);
				filename.setText(result);
			}
		});
		
		//TODO: make this not ugly
		filename = new Label(container,SWT.BORDER | SWT.SINGLE);
		filename.setText("                                                                                                                                                                        ");
	}
	
	public void createSurveyField(Composite parent) {
		Label label1 = new Label(parent, SWT.None);
		label1.setText("Enter survey to fill after modelling is finished (optional): ");
		survey = new Text(parent, SWT.BORDER | SWT.SINGLE);
		survey.setText("");
		survey.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
			
			}

		});
		
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		survey.setLayoutData(gd);
	}
	
	public String getName() {
		return name.getText();
	}
	
	public String getDirectory() {
		return directory.getText();
	}
	
	public String getTime() {
		return time.getText();
	}

	public String getFileName() {
		return filename.getText();
	}
	
	public String getSurveryLink() {
		return survey.getText();
	}

	private void pageComplete() {
		for (boolean b : finished) {
			if (!b) {
				setPageComplete(false);
				return;
			}
		}
		setPageComplete(true);
	}
}
