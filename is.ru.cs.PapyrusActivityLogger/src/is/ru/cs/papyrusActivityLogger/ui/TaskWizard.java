package is.ru.cs.papyrusActivityLogger.ui;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

public class TaskWizard extends Wizard implements INewWizard{
	
	protected NewPageOne one;
	
	public TaskWizard() {
	}
	
	@Override
	public String getWindowTitle() {
		return "Export my data";
	}
	
	@Override
	public void addPages() {
		one = new NewPageOne();
		addPage(one);
	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		
	}
	
	@Override
	public boolean performFinish() {
		String name = one.getName();
		String directory = one.getDirectory();
		int time = Integer.parseInt(one.getTime());
		String modelFileName = one.getFileName();
		String surveryLink = one.getSurveryLink();

		TaskMaker tm = new TaskMaker();
		tm.makeTask(name, directory, time, modelFileName, surveryLink);
		//		int time = Integer.parseInt(one.getTime());
//		String filename = one.getFileName();
//		
//		PapyrusModelModificationListener listener = new PapyrusModelModificationListener();
//		listener.setTime(time);
//		listener.setLoggingFile(filename);
//
//		PapyrusLoggingCommand c = new PapyrusLoggingCommand();
//		c.getDomain().addResourceSetListener(listener);
		
		return true;
	}	
}
