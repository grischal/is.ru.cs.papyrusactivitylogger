package is.ru.cs.papyrusActivityLogger.ui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;
import org.json.JSONArray;
import org.json.JSONObject;

import is.ru.cs.papyrusActivityLogger.ListenerClass;
import is.ru.cs.papyrusActivityLogger.ListenerManager;
import is.ru.cs.papyrusActivityLogger.PapyrusModelModificationListener;
import is.ru.cs.papyrusActivityLogger.SurveyPopUpEnd;
import is.ru.cs.papyrusActivityLogger.commands.PapyrusLoggingCommand;

public class ImportFileMaker {

	private String projectFileContent1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
			+ "<projectDescription>\r\n"
			+ "	<name>";
	private String projectFileContent2 = "</name>\r\n"
			+ "	<comment></comment>\r\n"
			+ "	<projects>\r\n"
			+ "	</projects>\r\n"
			+ "	<buildSpec>\r\n"
			+ "	</buildSpec>\r\n"
			+ "	<natures>\r\n"
			+ "	</natures>\r\n"
			+ "</projectDescription>";
	
	
	public ImportFileMaker() {
		
	}

	public void make(String filePath, String workspaceDir, IWorkbench workbench) {
		File taskFile = new File(filePath);
		StringBuilder fileContent = new StringBuilder();

		try {	
			String content = Files.readString(taskFile.toPath());
			fileContent.append(content);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		JSONObject object = new JSONObject(fileContent.toString());

		StringBuilder sb = new StringBuilder(filePath);
		//This contains the separator
		String fileName = sb.substring(sb.lastIndexOf(File.separator),sb.lastIndexOf("."));
		
		//Might not be needed
		String newWorkspaceDir = workspaceDir.replace("/", File.separator);
		File projectFile = new File(newWorkspaceDir + fileName);
		
		//Maybe we need to check if we actually made the dir?
		projectFile.mkdir();

		//Write the .project file 
		//(maybe ResourcesPlugin.getWorkspace().newProjectDescription(projectFileName) does the same thing)
		try {
			File dotProjectFile = new File(projectFile.getAbsolutePath() + File.separator + ".project");
			dotProjectFile.createNewFile();
			
			FileWriter writer = new FileWriter(dotProjectFile.getAbsoluteFile());
			
			//Remove the separator from the string variable
			fileName = fileName.substring(File.separator.length());
			
			writer.write(projectFileContent1 + fileName + projectFileContent2);
			writer.flush();
			writer.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JSONArray arr = (JSONArray) object.get("modelFiles");
		String diFile = "";
		if (arr.isEmpty()) {
			diFile = "model.di";
			createEmptyModel(projectFile.getAbsolutePath());
		}
		else {
			//Write all the model files (including diagrams)
			for (Object modelObject: arr) {
				JSONObject jsonModelObject = (JSONObject) modelObject;
				if (jsonModelObject == null) {
					throw new NullPointerException("Unable to cast to JSONObject");
				}
				for (String modelFileName: jsonModelObject.keySet()) {
					try {
						File file = new File(projectFile.getAbsolutePath() + File.separator + modelFileName);
						file.createNewFile();
						
						FileWriter writer = new FileWriter(file.getAbsoluteFile());
						String modelFileContent = (String) jsonModelObject.get(modelFileName);
						writer.write(modelFileContent);
						writer.flush();
						writer.close();
						if (modelFileName.contains(".di")) {
							diFile = modelFileName;
						}
						
						
					} catch (IOException e) {
						e.printStackTrace();
					}
				}	
			}
		}
		
		//Make eclipse see the new file
		IProject project = addProject(projectFile.getAbsolutePath().toString() + File.separator + ".project");
		
		//Open the diagram and add a listener with the given time and link
		openProject(project, workbench, object.getInt("minutes"), object.getString("survey"), diFile);

	}
	
	private IProject addProject(String projectFileName) {
		try {
			IProjectDescription description = ResourcesPlugin.getWorkspace().loadProjectDescription(new Path(projectFileName));
			IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(description.getName());
			project.create(description, null);
			project.open(null);;
			return project;
			
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	private void openProject(IProject project, IWorkbench workbench, int minutes, String survey, String diFile) {
		IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		IWorkbenchPage page = window.getActivePage();

		try {
			IDE.openEditor(page,project.getFile(diFile),true);
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
 		//Creating the listenerClass that contains information on how long the listener should be active,
		//what its attached o and what to do after the time is up
		PapyrusLoggingCommand c = new PapyrusLoggingCommand();
		PapyrusModelModificationListener listener = new PapyrusModelModificationListener();
		TransactionalEditingDomain attachedTo = c.getDomain();
		SurveyPopUpEnd end = new SurveyPopUpEnd(survey);
		
		ListenerClass listenerClass = new ListenerClass(listener,attachedTo,minutes*60000,end);
		ListenerManager.addListener(listenerClass);
		
		//Change the perspective to the papyrus perspective
		IPerspectiveDescriptor[] ps = page.getOpenPerspectives();
		for (IPerspectiveDescriptor p : ps) {
			if (p.getId() == "org.eclipse.papyrus.infra.core.perspective") {
				page.setPerspective(p);
				break;
			}
		}
		
//		//Opening the TaskView
//		try {
//			TaskView view = (TaskView) page.showView("is.ru.cs.PapyrusActivityLogger.taskView");
//
//			TaskManager.addTask(new ModellingTask("model1", 10000, "this is model task", view, "modeluri"));
//			TaskManager.addTask(new BreakTask("break1", 10000, "this is a break", view));
//			TaskManager.addTask(new URLTask("url1", 10000, "this is a url thing", view, "https://www.google.com/"));
//			
//		} catch (PartInitException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	private void createEmptyModel(String projectPath) {
		String diFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
				+ "<architecture:ArchitectureDescription xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:architecture=\"http://www.eclipse.org/papyrus/infra/core/architecture\" contextId=\"org.eclipse.papyrus.infra.services.edit.TypeContext\"/>\r\n";
		String notationFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
				+ "<xmi:XMI xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\"/>\r\n";
		String umlFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
				+ "<uml:Model xmi:version=\"20131001\" xmlns:xmi=\"http://www.omg.org/spec/XMI/20131001\" xmlns:uml=\"http://www.eclipse.org/uml2/5.0.0/UML\" xmi:id=\"_c68XgNOTEeu_yPNSeHW1Og\" name=\"model2\">\r\n"
				+ "  <packageImport xmi:type=\"uml:PackageImport\" xmi:id=\"_c9iYgNOTEeu_yPNSeHW1Og\">\r\n"
				+ "    <importedPackage xmi:type=\"uml:Model\" href=\"pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#_0\"/>\r\n"
				+ "  </packageImport>\r\n"
				+ "</uml:Model>\r\n";

		HashMap<String, String> modelFiles = new HashMap<String,String>();
		modelFiles.put("model.di", diFile);
		modelFiles.put("model.notation", notationFile);
		modelFiles.put("model.uml", umlFile);

		for (String modelFileName: modelFiles.keySet()) {
			File file = new File(projectPath + File.separator + modelFileName);
			try {
				file.createNewFile();
				FileWriter writer = new FileWriter(file.getAbsoluteFile());
				String modelFileContent = modelFiles.get(modelFileName);
				writer.write(modelFileContent);
				writer.flush();
				writer.close();			
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
