package is.ru.cs.papyrusActivityLogger.ui;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.mylyn.commons.ui.dialogs.AbstractNotificationPopup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class NotificationPopUpUI extends AbstractNotificationPopup {
//public class NotificationPopUpUI {
	private String popUpText = "";
	
	public NotificationPopUpUI(Display display) {
		super(display);
//		 TODO Auto-generated constructor stub
	}
	
//	@Override
	public void createContentArea(Composite composite){
		//TODO: fix wrapping
 		GridLayout layout = new GridLayout();
		composite.setLayout(layout);
		String text = this.popUpText;
		Link surveyLink = new Link(composite, SWT.WRAP);
		surveyLink.setText(text);
		Point size = surveyLink.computeSize(200, SWT.DEFAULT );
		System.out.println(size);
		surveyLink.setSize(size);
		GridData data = new GridData(SWT.FILL);
		surveyLink.setLayoutData(data);

		surveyLink.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					//Getting the default device browser and opening the survey
					PlatformUI.getWorkbench().getBrowserSupport().getExternalBrowser().openURL(new URL(e.text));
				} catch (PartInitException | MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		composite.setSize(size);
		surveyLink.getParent().layout();
	}
	
//	@Override 
	public String getPopupShellTitle() {
		return "Survey fill form";
	}

	public void setSurveyLink(String survey) {
		//Adding the protocol
		if (!survey.startsWith("http://") || !survey.startsWith("https://")) {
			survey = "http://" + survey;
		}
		setPopUpText("This is a link to <a href=\"" + survey + "\">the survey</a>");
	}
	
	public void setPopUpText(String text) {
		this.popUpText = text;
	}
}
