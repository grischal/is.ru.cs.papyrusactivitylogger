package is.ru.cs.papyrusActivityLogger.ui;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;

/**
 * Wizard page that lets the user select a .json task file
 * @author Alexander
 *
 */
public class ImportPageOne extends WizardPage {

	private Composite container;
	private Label filename;
	
	protected ImportPageOne() {
		super("First Page");
		
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.None);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;
		
		createFileSelection(container);
		
		setControl(container);
		setPageComplete(false);
		
	}

	/**
	 * Creates a file selection field in the given parent
	 * @param parent  Composite, where the file selection field gets added
	 */
	private void createFileSelection(Composite parent) {
		//Adding the description
		Label label = new Label(container, SWT.None);
		label.setText("Select the .json file containing the tasks information: ");
		
		//Making the description take up 2 columns in the grid
		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.horizontalSpan = 2;
		label.setLayoutData(gridData);
		
		//Adding the file selection button
		Button fileSelect = new Button(container, SWT.BUTTON1);
		fileSelect.setText("Browse");
		fileSelect.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseUp(MouseEvent e) {
				// Opens a standard file selection window and allows the user to select *.json files
				FileDialog dialog = new FileDialog(getShell(), SWT.OPEN);
				dialog.setFilterExtensions(new String [] {"*.json"});
				//Saving the file selected as a variable
				String result = dialog.open();
				
				//Update the file selection box text 
				filename.setText(result);
				//Resetting the file selection box layout since the text might be smaller/larger now
				filename.getParent().layout();
				
				//Allow the user to finish the editor
				setPageComplete(true);
			}
		});
		
		//Adding a box showing which file has been selected
		filename = new Label(container,SWT.BORDER | SWT.SINGLE);
		filename.setText("");
	}
	
	/**
	 * Used by TaskImportWizard to get the selected task file.
	 * @return  the path to the selected task file
	 */
	public String getFileName() {
		return filename.getText();
	}
}
