package is.ru.cs.papyrusActivityLogger.ui;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import is.ru.cs.papyrusActivityLogger.Task;
import is.ru.cs.papyrusActivityLogger.TaskManager;
import is.ru.cs.papyrusActivityLogger.extensions.TaskFactory;

/**
 * Contains static methods to handle task creating.
 * @author Alexander
 *
 */
public class ImportTaskMaker {
	private static ArrayList<Task> taskArray;
	private static String taskDir;

	/**
	 * Creates tasks and adds them to the TaskManager.
	 * Information about the tasks is in the json file at the given path (taskFilePath).
	 * In case of an error in the json file no tasks will be scheduled.
	 * @param taskFilePath  the path to the json file containing task information.
	 * @throws Exception  when the task could not be created.
	 */
	public static void makeTasks(String taskFilePath) throws Exception {
		// Getting the task view (and opening it)
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		TaskView view = (TaskView) page.showView("is.ru.cs.PapyrusActivityLogger.taskView");
		
		//Update the active task view in the task manager
		TaskManager.updateTaskView(view);

		// Opening the json file and getting the task array
		JSONObject object = new JSONObject(Files.readString(new File(taskFilePath).toPath()));
		JSONArray jsonTaskArray = object.getJSONArray("tasks");
		
		// Getting the directory of the task file
		StringBuilder sb = new StringBuilder(taskFilePath);
		taskDir = sb.substring(0,sb.lastIndexOf(File.separator));
		
		// Creating the array to hold the tasks which will then be added to the task manager
		// This prevents us from scheduling tasks in case of an error
		taskArray = new ArrayList<Task>();
		
		//Creating a HashMap to connect the json type to task factories
		HashMap<String, TaskFactory> factories = getFactories();
		
		// Initialise logging
		configureLogback(taskFilePath);
		
		// Iterating over the tasks and adding the to the task array
		for (Object task: jsonTaskArray) {
			JSONObject taskJSON = (JSONObject) task;
			String taskType = taskJSON.getString("type");

			// Getting the TaskFactory registered to this task type and using it to generate the task
			if (factories.containsKey(taskType)) {
				taskArray.add(factories.get(taskType).createTask(taskJSON));
			}
			else {
				// Undefined task type
				throw new Exception("Corresponding TaskFactory not found");
			}
		}

		// Schedule the tasks since there was no error
		for (Task task : taskArray) {
			TaskManager.addTask(task);
		}
	}
	
	/**
	 * Used to get a key (type) and value (factory) pair of the TaskFactories registered to the is.ru.cs.papyrusActivityLogger.task.factory extension point.
	 * The type is intended to be the same type used in the task json file.
	 * @return  a map containing the type and factories connected to that type.
	 */	
	private static HashMap<String, TaskFactory> getFactories(){
		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor("is.ru.cs.papyrusActivityLogger.task.factory");
		HashMap<String, TaskFactory> factories = new HashMap<String, TaskFactory>();
		int i = 0;
		while (i < config.length) {
			IConfigurationElement e = config[i];
			try {
				Object theClass = e.createExecutableExtension("class");
				if (theClass instanceof TaskFactory) {
					TaskFactory factory = (TaskFactory) theClass;
					factory.setTaskDirectory(taskDir);
					factories.put(e.getAttribute("type"), (TaskFactory) factory);
				}
				i++;
			} catch (CoreException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return factories;
	}
	
	private static void configureLogback(String taskFile) {
		try {
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        JoranConfigurator jc = new JoranConfigurator();
        jc.setContext(context);
        context.reset();
        
        //Set filename for logging to file.
        Date date = new Date();  
        SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyyhhmmss");  
        String strDate = formatter.format(date);  
    	
        context.putProperty("file-name", taskFile + "_" + strDate + ".log");
        URL logbackConfigFileUrl = FileLocator.find(Platform.getBundle("is.ru.cs.papyrusActivityLogger"), new Path("logback.xml"),null);
        jc.doConfigure(logbackConfigFileUrl.openStream());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JoranException e) {
			e.printStackTrace();
		}
    }
}
