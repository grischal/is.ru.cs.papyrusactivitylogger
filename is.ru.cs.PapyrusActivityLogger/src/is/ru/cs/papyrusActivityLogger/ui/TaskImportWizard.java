package is.ru.cs.papyrusActivityLogger.ui;

import java.io.IOException;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PartInitException;
import org.json.JSONException;

/**
 * An import wizard used to allow the user to select the .json task file.
 * @author Alexander
 *
 */
public class TaskImportWizard extends Wizard implements IImportWizard {

	private ImportPageOne one;
	
	/**
	 * 
	 */
	public TaskImportWizard() {
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {

	}

	/**
	 * Uses the ImportTaskMaker.makeTasks method to create the tasks.
	 * In case of an invalid .json file the wizard is not finished.
	 */
	@Override
	public boolean performFinish() {
		//Gets the task information from the provided .json file, creates the tasks and adds them to the task manager
		try {
			ImportTaskMaker.makeTasks(one.getFileName());
			//There was no exception
			return true;
		} catch (PartInitException | JSONException | IOException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			//Go here if the type was undefined
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Adds ImportPageOne as a page to this wizard.
	 */
	@Override
	public void addPages() {
		//Adding the pages needed for the import wizard
		one = new ImportPageOne();
		addPage(one);
	}
	
}
