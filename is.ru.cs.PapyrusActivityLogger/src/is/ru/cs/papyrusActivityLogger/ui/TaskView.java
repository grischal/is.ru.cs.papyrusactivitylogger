package is.ru.cs.papyrusActivityLogger.ui;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import is.ru.cs.papyrusActivityLogger.Model;
import is.ru.cs.papyrusActivityLogger.TaskManager;

/**
 * Displays information about the active task to the user.
 * This view is automatically opened if the ImportTaskMaker when the user import a study with the TaskImportWizard.
 * Tasks should call methods to update the TaskView when they start/end.
 * @author Alexander
 *
 */
public class TaskView extends ViewPart {
	
	private Composite parent;
	private Label nameLabel;
	private Label descriptionLabel;
	private Link link;
	private Button nextButton;
	private boolean nextButtonPressed;
	private ArrayList<Button> modelOpeningButtons;
	private static HashMap<String, Boolean> linksPressed;
	
	public TaskView() {
		super();
	}
	
	@Override
	public void createPartControl(Composite parent) {
		this.parent = new Composite(parent, SWT.WRAP);
		GridLayout layout = new GridLayout();
		this.parent.setLayout(layout);
		layout.numColumns = 1;
//		layout.horizontalSpacing=100;

		//Adding labels, link and button to the task view
		createNameLabel(this.parent,"");
		createDescriptionLabel(this.parent,"");
		createLink(this.parent,"");
		createNextButton(this.parent);
		
		this.modelOpeningButtons = new ArrayList<Button>();
		
		//Hiding the labels, link and button
		hideAll();
		
		//Since we created a new TaskView wew need to update the active task view in the TaskManager
		TaskManager.updateTaskView(this);
		System.out.println("Task view method called");
	}

	/**
	 * Adds a label to the given parent that displays the given name.
	 * @param parent  Composite that the label gets appended too.
	 * @param name  String which the label will display.
	 */
	private void createNameLabel(Composite parent, String name){
		nameLabel = new Label(parent,SWT.WRAP);

		//Grid data to allow for wrapping
	    GridData gridData = new GridData( SWT.WRAP ) ;
	    gridData.grabExcessHorizontalSpace = true ;
	    gridData.horizontalAlignment = SWT.FILL;
	    nameLabel.setLayoutData(gridData);
		
		nameLabel.setText("Task name: " + name);
	}

	/**
	 * Adds a label to the given parent that display the given description.
	 * @param parent  Composite that the label gets appended too.
	 * @param description  String which the label will display.
	 */
	private void createDescriptionLabel(Composite parent, String description) {
		descriptionLabel = new Label(parent,SWT.WRAP);
		
		//Grid data to allow for wrapping
	    GridData gridData = new GridData( SWT.WRAP ) ;
	    gridData.grabExcessHorizontalSpace = true ;
	    gridData.horizontalAlignment = SWT.FILL;
		descriptionLabel.setLayoutData(gridData);
	    
		descriptionLabel.setText("Task description: " + description);		
	}
	
	
	/**
	 * Adds a button the the given parent. 
	 * When the button is pressed the TaskManager.checkTasks() method is automatically called.
	 * @param parent  Composite that the button gets appended too.
	 */
	public void createNextButton(Composite parent) {
		nextButtonPressed = false;
		nextButton = new Button(parent,0);
		nextButton.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				
			}

			@Override
			public void mouseDown(MouseEvent e) {
				
			}

			@Override
			public void mouseUp(MouseEvent e) {
				nextButtonPressed = true;
				TaskManager.checkTasks();
			}
			
		});
		nextButton.setText("Next Task");
	}
	
	
	/**
	 * Adds a Link to the parent.
	 * When the Link is pressed it opens the given link in the devices default browser.
	 * @param parent  Composite that the link gets appended too.
	 * @param link  String of the link that gets opened.
	 */
	public void createLink(Composite parent, String link) {
		String text = "<a href=\"" + link + "\">Link</a>";
		this.link = new Link(parent, SWT.WRAP );
		
		//Grid data to allow for wrapping
		GridData gridData = new GridData( SWT.WRAP ) ;
		gridData.grabExcessHorizontalSpace = true ;
		gridData.horizontalAlignment = SWT.FILL;
		this.link.setLayoutData(gridData);

		this.link.setText(text);
		
		this.link.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					//Getting the default device browser and opening the survey
					PlatformUI.getWorkbench().getBrowserSupport().getExternalBrowser().openURL(new URL(e.text));
					System.out.println(e.text);
				} catch (PartInitException | MalformedURLException e1) {
					e1.printStackTrace();
				}
				//Store that this link has been pressed in the HashMap
				linksPressed.replace(e.text, true);
				//Check whether we should enable the nextButton
				checkNextButton();
			}
		});
	}
	
	/**
	 * Enables the next button if all links have been pressed.
	 * A link has been pressed if its value in the linksPressed HashMap is true
	 */
	protected void checkNextButton() {
		boolean allPressed = true;
		for (String link : linksPressed.keySet()) {
			if (!linksPressed.get(link)) {
				allPressed = false;
				break;
			}
		}
		if (allPressed) {
			nextButton.setEnabled(true);
		}
	}

	/**
	 * Provides an easy way to check if the next button has been pressed since it was last updated.
	 * Called from tasks to check if the task is over.
	 * @return false if the button has not been created or has not been pressed since the last update. 
	 * true if the button has been pressed since the last update.
	 */
	public boolean nextButtonPressed() {
		return nextButton != null && nextButtonPressed;
	}
	
	/**
	 * Reveals the name Label and sets its text value to the given name.
	 * @param name  String that the name Label's text gets set to.
	 */
	public void updateName(String name) {
		nameLabel.setText("Task name: " + name);
		nameLabel.setVisible(true);
		//Resize the window (so the text fits)
		nameLabel.getParent().layout();
	}

	/**
	 * Reveals the description Label and sets its text value to the given description.
	 * @param description  String that the description Label's text gets set to.
	 */
	public void updateDescription(String description) {
		descriptionLabel.setText("Task description: " + description);
		descriptionLabel.setVisible(true);
		//Resize the window (so the text fits)
		descriptionLabel.getParent().layout();
	}
	
	/**
	 * Reveals the Link and sets its href to the given link.
	 * @param link  String that the Link's href gets set to.
	 */
	public void updateLink(String link) {
		this.link.setText("<a href=\"" + link + "\">" + link + "</a>");
		this.link.setVisible(true);
		//Resize the window (so the text fits)
		this.link.getParent().layout();
	}
	
	/**
	 * Reveals multiple links, each in a separate line, and sets the href.
	 * @param links  list of strings to reveal as links
	 */
	public void updateLink(List<String> links) {
		StringBuilder sb = new StringBuilder();
		linksPressed = new HashMap<String, Boolean>();
		for (String link : links) {
			sb.append("<a href=\"" + link + "\">" + link + "</a>\n");
			linksPressed.put(link, false);
		}
		this.link.setText(sb.toString());
		this.link.setVisible(true);
		//Resize the window (so the text fits)
		this.link.getParent().layout();
	}
	
	//Revealing the next button and disable/enable it depending on whether the link needs to be pressed or not
	/**
	 * Reveals the next button.
	 * Enables or disables the button depending on whether the urlRequired is set to false or true.
	 * If the button starts disabled (urlRequired is true) its gets enabled when all links in the Link are pressed.
	 * If this method is called with urlRequired true a Link should exist that can be pressed.
	 * @param urlRequired  boolean indicating whether the button should start enabled or not.
	 */
	public void updateNextButton(boolean urlRequired) {
		//TODO: When url is pressed then the task view is closed and reopened the button will be grayed out again
		nextButton.setVisible(true);
		nextButtonPressed = false;
		if (urlRequired) {
			nextButton.setEnabled(false);			
		} else {
			nextButton.setEnabled(true);
		}
//		//Resize the window (so the text fits)
//		nextButton.getParent().layout();
	}
	
	/**
	 * Reveals model opening buttons for every model in the given list.
	 * These buttons should be used to reopen a model the user closed.
	 * Buttons call the model.open method to reopen the model.
	 * @param models  the models for which to add opening buttons
	 */
	public void updateModelOpeningButton(List<Model> models) {
		for (Model model : models) {
			addModelOpeningButton(model);
		}
		parent.layout();
	}
	
	/**
	 * Adds a button to open the given model.
	 * @param model  the model to open.
	 */
	private void addModelOpeningButton(Model model) {
		Button button = new Button(parent,0);
		button.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				
			}

			@Override
			public void mouseDown(MouseEvent e) {
				
			}

			@Override
			public void mouseUp(MouseEvent e) {
				try {
					model.open();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});
		button.setText(model.getName());
		this.modelOpeningButtons.add(button);
	}

	/**
	 * Hides all UI elements.
	 */
	public void hideAll() {
		nameLabel.setVisible(false);
		descriptionLabel.setVisible(false);
		link.setVisible(false);
		nextButton.setVisible(false);
		removeModelOpeningButtons();
	}
	
	private void removeModelOpeningButtons() {
		for (Button button : modelOpeningButtons) {
			button.dispose();
		}
		modelOpeningButtons = new ArrayList<Button>();
	}

	@Override
	public void setFocus() {
		
	}

}
