package is.ru.cs.papyrusActivityLogger.ui;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * Contains a static method to handle pop-ups.
 * Called from tasks which are timed.
 * Used to inform the user that the task has ended.
 * @author Alexander
 */
public class TaskEndNotification {
	/**
	 * Displays a information pop-up containing the given information.
	 * Intended to be called in timed tasks to inform the user that the task has ended.
	 * @param information  String containing the information to show in the pop-up box.
	 */
	public static void openInformationBox(String information) {
		Display display = Display.getCurrent();
		if (display == null) {
			display = Display.getDefault();
		}
		Shell shell = new Shell(display);
		MessageDialog.openInformation(shell, "Information", information);
	}
}
