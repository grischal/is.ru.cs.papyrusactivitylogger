package is.ru.cs.papyrusActivityLogger.ui;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

import org.json.JSONObject;

public class TaskMaker {
	public TaskMaker() {
		
	}
	
	public void makeTask(String name, String directory,int minutes, String modelFileName, String surveyLink) {
		JSONObject json = new JSONObject();
		json.put("minutes", minutes);
		json.put("survey", surveyLink);
		putModelData(json, modelFileName);
		saveToFile(json,name,directory);
	}
	
	private void putModelData(JSONObject json, String modelFileName) {
		if (modelFileName.trim().equals("")) {
			json.put("modelFiles", new ArrayList<JSONObject>());
			return;
		}
		StringBuilder sb = new StringBuilder(modelFileName);
		int lastSeparator = sb.lastIndexOf(File.separator);
		String path = sb.substring(0, lastSeparator);
		
		int lastDot = sb.lastIndexOf(".");
		String modelName = sb.substring(lastSeparator+1,lastDot);
		
		System.out.println(path);
		System.out.println(modelName);
		
		File dir = new File(path);
		File[] foundFiles = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.startsWith(modelName);
		    }
		});
		
		ArrayList<JSONObject> files = new ArrayList<JSONObject>();
		
		for (File file: foundFiles) {
			StringBuilder fileSB = new StringBuilder();
			try {
				String content = Files.readString(file.toPath());
				fileSB.append(content);
				
				//Create a new json object and add to it (filename, filecontent)
				JSONObject newJson = new JSONObject();
				newJson.put(file.getName(), fileSB.toString());
				//Adding to the files array the new json object
				files.add(newJson);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		json.put("modelFiles", files);
	}
	
	private void saveToFile(JSONObject json, String name, String directory) {
		StringBuilder sb = new StringBuilder();
		sb.append(directory);
		sb.append(File.separator);
		sb.append(name);
		sb.append(".json");

		try {
			File file = new File(sb.toString());
			file.createNewFile();
			
			FileWriter writer = new FileWriter(sb.toString());
			String jsonString = json.toString(1);
			writer.write(jsonString);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
