package is.ru.cs.papyrusActivityLogger;

import java.io.File;
import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

/**
 * Contains methods to open this model and adding/removing listeners on the model.
 * Used in TaskView to create a model opening button.
 * Called from ModellingTask to remove the listener.
 * Created in ModellingTask.
 * @author Alexander
 */
public class Model {

	private String modelURI;
	private String editorID;
	private TransactionalEditingDomain attachedTo;
	private PapyrusModelModificationListener listener;
	private IEditorPart editor;
	private static IEditorRegistry registry = PlatformUI.getWorkbench().getEditorRegistry();
	private static HashMap<TransactionalEditingDomain, PapyrusModelModificationListener> domainListenerMap = 
			new HashMap<TransactionalEditingDomain, PapyrusModelModificationListener>();
	
	/**
	 * Called in ModellingTask.
	 * @param modelURI  the URI this model should open.
	 */
	public Model(String modelURI) {
		this.modelURI = modelURI;
	}
	
	/**
	 * Opens the model with the first registered editor for a that file.
	 * Gets the domain linked to that editor.
	 * Adds a listener to the domain if its a TransactionalEditingDomain
	 * Does not add multiple listeners to the same domain.
	 * @throws Exception  when the domain is not a TransactionalEditingDomain or not found.
	 */
	public void open() throws Exception {
		//Get the currently active page
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		//Change the modelURI String to an actual URI
		java.net.URI uri = new File(this.modelURI).toURI();
		
		//Get the default editorID of the model file
		IEditorDescriptor [] desc = registry.getEditors(this.modelURI);
		//Get the first editorID available
		editorID = desc[0].getId();
		try {
			//Open the model with the default editorID
			IEditorPart editor = IDE.openEditor(page, uri, editorID, true);
			//Make sure we don't add another listener to the same domain 
			//Not this only stops us from adding a listener from this Model.open method but not from other methods
			if (editor != this.editor) {
				//Get the domain
				Object domain = AdapterFactoryEditingDomain.getEditingDomainFor(editor);
				if (domain instanceof TransactionalEditingDomain) {
					attachedTo = (TransactionalEditingDomain) domain;
				}
				if (attachedTo == null) {
					throw new Exception();
				}
				
				// This check is important if we have multiple editors on the same domain (fx. the task editor)
				// We already have a listener registered on this domain
				if (domainListenerMap.containsKey(attachedTo)) {
					// Getting the listener form the map
					listener = domainListenerMap.get(attachedTo);
				}
				// We need to create a new listener and add it to the domain
				else {
					// Create a new listener and adding it to the domain
					listener = new PapyrusModelModificationListener();
					attachedTo.addResourceSetListener(listener);
					// Adding the connection to the map
					domainListenerMap.put(attachedTo, listener);
				}
				
				this.editor = editor;
			}
			
		} catch (PartInitException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Called in ModellingTask to remove the listener when the task is over
	 */
	public void removeListener() {
		if (domainListenerMap.containsKey(attachedTo)) {
			attachedTo.removeResourceSetListener(listener);
			domainListenerMap.remove(attachedTo);
		}
	}
	
	/**
	 * Called in TaskView to set the name of the button to open this model
	 * @return  the name of the model
	 */
	public String getName() {
		return modelURI.substring(modelURI.lastIndexOf(File.separator) + 1);
//		StringBuilder sb = new StringBuilder(modelURI);
//		return sb.substring(sb.lastIndexOf(File.separator) + 1);
	}
}
