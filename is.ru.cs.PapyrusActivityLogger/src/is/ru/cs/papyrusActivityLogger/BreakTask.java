package is.ru.cs.papyrusActivityLogger;

import is.ru.cs.papyrusActivityLogger.ui.TaskEndNotification;
import is.ru.cs.papyrusActivityLogger.ui.TaskView;

/**
 * Used to create a break in a study.
 * Controlled by the TaskManager.
 * Created in the ImportTaskMaker.
 * @author Alexander
 *
 */
public class BreakTask extends Task{

	private long startTime;
	
	/**
	 * 
	 * @param name  name of the task.
	 * @param time  time to keep this task active, -1 if it should be an untimed task.
	 * @param description  description of the task.
	 */
	public BreakTask(String name, long time, String description) {
		super(name, time, description);
	}

	/**
	 * Checks if this task has ended.
	 * Checks the nextButtonPressed of the task view if this was an untimed task.
	 * Checks if the time is up if the was a timed task.
	 */
	@Override
	public boolean taskOver() {
		//Infinite time break
		if (this.time ==  -1) {
			//Check if the next button is pressed
			return started && this.taskView.nextButtonPressed();
		} else {			
			//Check if the time is over
			return started && (System.currentTimeMillis() - startTime) > time;			
		}
	}

	/**
	 * Hides all UI elements in taskView.
	 * Displays a pop-up indicating the task is over if this was a timed task.
	 */
	@Override
	public void end() {
		taskView.hideAll();		
		System.out.println("In break end");
		
		//Check that the time is not -1 (meaning the task ended because of a timeout not because of a next button press)
		if (this.time !=  -1) {
			//Displaying a popup indicating that the task is now over
			TaskEndNotification.openInformationBox("The break task has now ended. Please refer to the task view for instructions on a new task");
//			NotificationPopUpUI popup = new NotificationPopUpUI(Display.getDefault());
//			popup.setPopUpText("This task has now ended, please refer to the task view for instructions on a new task");
//			popup.setFadingEnabled(false);
//			popup.open();
		}
	}

	/**
	 * Called to start this task form TaskManager.
	 * Calls updateTaskView. 
	 * Starts the task timer.
	 */
	@Override
	public void start(TaskView taskView) {
		startTime = System.currentTimeMillis();
		started = true;
		updateTaskView(taskView);
		
		System.out.println("In break start");
	}

	/**
	 * Called to reveal all relevant UI parts for this task.
	 * Displays the name and description labels. Displays the next button if this is an untimed break.
	 */
	@Override
	public void updateTaskView(TaskView taskView) {
		//Adding a name and description to the task view
		taskView.updateName(name);
		taskView.updateDescription(description);
		
		//Time is -1 indicates that this is an infinite time task
		if (this.time ==  -1) {
			//Add a button to the task view that takes us to the next task
			//Don't require a some URL to be pressed to allow the button press
			taskView.updateNextButton(false);
		}
		
		//Updating the task taskView
		this.taskView = taskView;
	}

}
