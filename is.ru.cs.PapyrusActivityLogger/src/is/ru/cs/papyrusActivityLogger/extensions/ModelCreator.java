package is.ru.cs.papyrusActivityLogger.extensions;

/**
 * Interface to be implemented for the model creation extension point.
 * @author Alexa
 *
 */
public interface ModelCreator {
	/**
	 * Creates the model in the given directory with the given name and returns the absolute path to the model.
	 * @param directory  the directory in which to create the model
	 * @param name  the name of the model
	 * @return  the absolute URI of the created model
	 */
	public String createModel(String directory, String name);
}
