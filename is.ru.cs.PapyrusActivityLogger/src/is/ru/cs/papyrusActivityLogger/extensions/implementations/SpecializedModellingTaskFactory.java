package is.ru.cs.papyrusActivityLogger.extensions.implementations;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import is.ru.cs.papyrusActivityLogger.SpecializedModellingTask;
import is.ru.cs.papyrusActivityLogger.Task;
import is.ru.cs.papyrusActivityLogger.extensions.TaskFactory;

/**
 * Used by ImportTaskMaker to create the ModellingTask type.
 * Registered to the is.ru.cs.papyrusActivityLogger.task.factory extension point.
 * @author Alexander
 */
public class SpecializedModellingTaskFactory extends TaskFactory {

	@Override
	public Task createTask(JSONObject task) throws IOException {
		String name = task.getString("taskName");
		int minutes = task.getInt("minutes");
		String description = task.getString("description");
		Object model = task.get("modelURI");
		
		//Only a single model
		if (model instanceof String) {
			// This is a relative path so we need to combine them
			String modelURI = getAbsolutePath((String) model);
			return new SpecializedModellingTask(name, minutes, description, modelURI);
		}
		
		//Multiple models
		else if (model instanceof JSONArray) {
			JSONArray models = (JSONArray) model;
			List<String> modelURIs = new ArrayList<String>();
			int i = 0;
			// Adding modelURIs in the JSONArray to another string list
			while (!models.isNull(i)) {
				String modelURI = models.getString(i);
				// This is a relative path so we need to combine them
				modelURI = getAbsolutePath(modelURI);
				modelURIs.add(modelURI);
				i++;
			}
			return new SpecializedModellingTask(name, minutes, description, modelURIs);
		}
		
		return null;
	}
	
	/**
	 * Used to calculate the absolute path to some file from a relative path.
	 * Requires the caller to first call setTaskDirectory() to properly handle the relative path.
	 * @param relativePath  the relative path from the taskDirectory.
	 * @return  the absolute path from the file.
	 * @throws IOException  if the file is not found.
	 */
	private String getAbsolutePath(String relativePath) throws IOException {
		relativePath.replace("/", File.separator);
		return new File(this.taskDirectory + File.separator + relativePath).getCanonicalPath();
	}
	
}
