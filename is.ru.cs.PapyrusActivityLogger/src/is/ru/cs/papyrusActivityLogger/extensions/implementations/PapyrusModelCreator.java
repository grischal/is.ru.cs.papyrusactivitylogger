package is.ru.cs.papyrusActivityLogger.extensions.implementations;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import is.ru.cs.papyrusActivityLogger.extensions.ModelCreator;

/**
 * Example implementation of the ModelCreator that creates empty models for papyrus.
 * @author Alexa
 *
 */
public class PapyrusModelCreator implements ModelCreator {

	@Override
	public String createModel(String directory, String name) {
		try {
			return createEmptyPapyrusModel(directory, name, 0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ERROR";
		}
	}
	
	/**
	 * Creates an empty Papyrus model with the given name in the given directory.
	 * Intended to be called when a modeling task does not specify a model URI.
	 * Empty model files get the name of the task. 
	 * If that name is taken it tries to create a task with the name task + (1) then task + (2) etc.
	 * Note: Currently eclipse complains about a lack of a .sash file.
	 * @param directory  String, directory in which the model will be created
	 * @param modelName  String, the name of the model
	 * @throws IOException
	 */
	private String createEmptyPapyrusModel(String directory, String name, int i) throws IOException {
		//Hard-coded data for empty model files
		String diFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
				+ "<architecture:ArchitectureDescription xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:architecture=\"http://www.eclipse.org/papyrus/infra/core/architecture\" contextId=\"org.eclipse.papyrus.infra.services.edit.TypeContext\"/>\r\n";
		String notationFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
				+ "<xmi:XMI xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\"/>\r\n";
		String umlFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
				+ "<uml:Model xmi:version=\"20131001\" xmlns:xmi=\"http://www.omg.org/spec/XMI/20131001\" xmlns:uml=\"http://www.eclipse.org/uml2/5.0.0/UML\" xmi:id=\"_c68XgNOTEeu_yPNSeHW1Og\" name=\"model2\">\r\n"
				+ "  <packageImport xmi:type=\"uml:PackageImport\" xmi:id=\"_c9iYgNOTEeu_yPNSeHW1Og\">\r\n"
				+ "    <importedPackage xmi:type=\"uml:Model\" href=\"pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#_0\"/>\r\n"
				+ "  </packageImport>\r\n"
				+ "</uml:Model>\r\n";
		
		//Adding the filenames and file content to a HashMap
		HashMap<String, String> modelFiles = new HashMap<String,String>();
		modelFiles.put(".notation", notationFile);
		modelFiles.put(".di", diFile);
		modelFiles.put(".uml", umlFile);
		
		//Iterating over the files
		for (String extension: modelFiles.keySet()) {
			//Creating a new file (if it does not exist) in the given directory
			File file;
			if (i == 0) {
				file = new File(directory + File.separator + name + extension);				
			}
			else {
				file = new File(directory + File.separator + name + "(" + i + ")" + extension);
			}
			if (!file.createNewFile()) {
				//This file already exists so we try another file name
				return createEmptyPapyrusModel(directory, name, i+1);
			}
			
			//Writing the content to the file (overwrites if the file already existed)
			FileWriter writer = new FileWriter(file.getAbsolutePath());
			writer.write(modelFiles.get(extension));
			writer.flush();
			writer.close();
		}
		if (i == 0) {
			return directory + File.separator + name + ".di";
		}
		else {
			return directory + File.separator + name + "(" + i + ").di";
		}
	}
}
