package is.ru.cs.papyrusActivityLogger.extensions.implementations;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import is.ru.cs.papyrusActivityLogger.Task;
import is.ru.cs.papyrusActivityLogger.URLTask;
import is.ru.cs.papyrusActivityLogger.extensions.TaskFactory;

/**
 * Used by ImportTaskMaker to create the URLTask type.
 * Registered to the is.ru.cs.papyrusActivityLogger.task.factory extension point.
 * @author Alexander
 */
public class URLTaskFactory extends TaskFactory {

	@Override
	public Task createTask(JSONObject task) throws Exception {
		String name = task.getString("taskName");
		int minutes = task.getInt("minutes");
		String description = task.getString("description");
		Object URL = task.get("URL");
		Object protocol = null;
		try {
			protocol = task.get("protocol");			
		}
		catch (JSONException e){
			
		}
		// Only a single URL task
		if (URL instanceof String) {
			String url = (String) URL;
			String protocolString = (String) protocol;
			//Adding the protocol
			if (protocolString != null && !protocolString.equals("")) {
				url = addProtocol(url, protocolString);
			}
			else {
				url = addProtocol(url);
			}
			return new URLTask(name, minutes, description, url);	
		}
		// Multiple URL task
		else if (URL instanceof JSONArray) {
			JSONArray urls = (JSONArray) URL;
			JSONArray protocols = (JSONArray) protocol;
			List<String> stringUrls = new ArrayList<String>();
			int i = 0;
			// Adding urls in the JSONArray to another string list
			while (!urls.isNull(i)) {
				String url = urls.getString(i);
				String protocolString = null;
				if (protocols != null) {
					protocolString = protocols.getString(i);					
				}
				//Adding the protocol if needed
				if (protocolString != null && !protocolString.equals("")) {
					url = addProtocol(url, protocolString);
				}
				else {
					url = addProtocol(url);
				}
				stringUrls.add(url);
				i++;
			}
			return new URLTask(name, minutes, description, stringUrls);
		}
		
		return null;
	}

	/**
	 * Used to add the file:// if there is no protocol in the URL.
	 * Requires the caller to call the setTaskDirectory method to properly handle the relative path.
	 * @param url  the url, the relative path if there is no protocol.
	 * @return  the url, the absolute path to some file if there was no protocol.
	 * @throws IOException  if the file was not found.
	 */
	private String addProtocol(String url) throws IOException {
		if (!url.contains("://")) {			
			url.replace("/", File.separator);
			// Adding the file protocol and making this an absolute path
			return "file://" + new File(this.taskDirectory + File.separator + url).getCanonicalPath();
		}
		return url;
	}	
	
	/**
	 * Used to add the given protocol to the url.
	 * @param url  url to use.
	 * @param protocol  protocol to use.
	 * @return  a new url with the added protocol.
	 * @throws IOException  in case of a file protocol when the file was not found.
	 */
	private String addProtocol(String url, String protocol) throws IOException {
		if (protocol.equals("file")) {
			url.replace("/", File.separator);
			return "file://" + new File(this.taskDirectory + File.separator + url).getCanonicalPath();
		}
		return protocol + "://" + url;
	}

}
