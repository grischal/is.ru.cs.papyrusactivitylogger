package is.ru.cs.papyrusActivityLogger.extensions.implementations;

import org.json.JSONObject;

import is.ru.cs.papyrusActivityLogger.BreakTask;
import is.ru.cs.papyrusActivityLogger.Task;
import is.ru.cs.papyrusActivityLogger.extensions.TaskFactory;

/**
 * Used by ImportTaskMaker to create the BreakTask type.
 * Registered to the is.ru.cs.papyrusActivityLogger.task.factory extension point.
 * @author Alexander
 */
public class BreakTaskFactory extends TaskFactory {

	@Override
	public Task createTask(JSONObject task) throws Exception {
		String name = task.getString("taskName");
		int minutes = task.getInt("minutes");
		String description = task.getString("description");
		return new BreakTask(name, minutes, description);
	}

}
