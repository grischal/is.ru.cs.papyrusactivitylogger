package is.ru.cs.papyrusActivityLogger.extensions;

import org.json.JSONObject;

import is.ru.cs.papyrusActivityLogger.Task;
/**
 * Interface to be implemented for the is.ru.cs.papyrusActivityLogger.task.factory extension point.
 * @author Alexander
 */
public abstract class TaskFactory {
	protected String taskDirectory;
	
	/**
	 * Used to set the taskDirectory of the factory.
	 * Should be called for all factories that use relative paths.
	 * taskDirectory is used when calculating relative paths.
	 * @param taskDirectory  the directory of the task json file.
	 */
	public void setTaskDirectory(String taskDirectory) {
		this.taskDirectory = taskDirectory;
	}
	
	 /**
	  * Used by ImportTaskMaker to get the task corresponding to the JSONObject.
	  * @param task  the JSONObject containing the task information.
	  * @return  the Task which the JSONObject describes.
	  * @throws Exception  when the task could not be created.
	  */
	public abstract Task createTask(JSONObject task) throws Exception;
}
