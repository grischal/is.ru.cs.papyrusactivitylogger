package is.ru.cs.papyrusActivityLogger;

import java.util.ArrayList;
import java.util.Timer;

import org.eclipse.swt.widgets.Display;

import is.ru.cs.papyrusActivityLogger.ui.TaskView;

/**
 * Keeps track of the Tasks and the active TaskView.
 * Called from ImportTaskMaker (to add new tasks).
 * Called from TaskView (to update the active task view).
 * @author Alexander
 */
public class TaskManager {
	private static ArrayList<Task> tasks = new ArrayList<Task>();
	private static boolean timerStarted = false;
	private static TaskView activeTaskView;
	
	/**
	 * Adds a task to the task queue.
	 * If this is the first task to be added a timer is scheduled to automatically check the tasks every 5 seconds.
	 * @param task  Task to add the the task queue
	 */
	public static void addTask(Task task) {
		tasks.add(task);
		if (!timerStarted) {
			Timer timer = new Timer();
			timer.schedule(new CheckTasks(), 1000, 5000);
			timerStarted = true;
		}
	}

	/**
	 * Checks the task queue.
	 * If a task is currently active it checks it the task is over and ends it if that's the case.
	 * If no task is currently active it starts the next task in the queue.
	 */
	public static void checkTasks() {
		if (tasks.size() > 0) {
			Task task = tasks.get(0);
			if (task.started) {
				if (task.taskOver()) {
					Display.getDefault().asyncExec(new Runnable() {

						@Override
						public void run() {
							//Remove the UI components in the UI thread
							task.end();
						}
					});

					tasks.remove(task);
					//Call checkTasks to start the next task
					checkTasks();
				}
			}
			else {
				Display.getDefault().asyncExec(new Runnable() {

					@Override
					public void run() {
						//Update the UI components in the UI thread
						task.start(activeTaskView);
	
					}
				});
			}
		}
	}
	
	/**
	 * Updates the taskView of the task manager.
	 * Calls the updateTaskView method if a task is currently active to add information to the task view.
	 * @param taskView  TaskView the task view to replace the old task view
	 */
	public static void updateTaskView(TaskView taskView) {
		if (activeTaskView != taskView) {
			activeTaskView = taskView;
			if (tasks.size() > 0) {
				Task task = tasks.get(0);
				if (task.started) {
					task.updateTaskView(taskView);
				}
			}
		}
	}
}
