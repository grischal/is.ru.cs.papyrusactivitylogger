package is.ru.cs.papyrusActivityLogger;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains the notifications which belong to the same user 'action'.
 * User actions are differentiated if the calls to resourceSetChanged are a specified time apart.
 * Used in PapyrusModelModificationListener.
 * @author Alexander
 *
 */
public class UserAction {
	protected List<LoggingNotification> notifications;
	protected LocalTime timestamp;

	/**
	 * Create an empty array of LoggingNotifications
	 */
	public UserAction() {
		this.notifications = new ArrayList<LoggingNotification>();
		this.timestamp = LocalTime.now();
	}
	
	/**
	 * Adds the notification to the list of notifications.
	 * @param notification  LoggingNotification, notification to be added to the array�
	 */
	public void addNotification(LoggingNotification notification) {
		notifications.add(notification);
	}
	
	/**
	 * Get the notifications of this action.
	 * @return  The list of LoggingNotifications
	 */
	public List<LoggingNotification> getNotifications () {
		return notifications;
	}
	
}
