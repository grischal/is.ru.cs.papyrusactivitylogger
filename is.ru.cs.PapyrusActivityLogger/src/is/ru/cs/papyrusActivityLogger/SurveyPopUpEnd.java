package is.ru.cs.papyrusActivityLogger;

import org.eclipse.swt.widgets.Display;

import is.ru.cs.papyrusActivityLogger.ui.NotificationPopUpUI;

public class SurveyPopUpEnd extends ListenerEndHandling {

	private String survey;

	public SurveyPopUpEnd(String survey) {
		this.survey = survey;
	}
	
	@Override
	void performEnd() {
		doPopup();
	}
	
	private void doPopup() {
		//Shows a popup asking the user to open the survey provided with he set survey method
		//Only show the popup if the survey was provided
		if (this.survey != null && this.survey != "") {
			Display.getDefault().asyncExec(new Runnable() {

				@Override
				public void run() {
					NotificationPopUpUI popup = new NotificationPopUpUI(Display.getDefault());
					popup.setSurveyLink(survey);
					popup.setFadingEnabled(false);
					popup.open();
				}
				
			});
		}
	}	
}
