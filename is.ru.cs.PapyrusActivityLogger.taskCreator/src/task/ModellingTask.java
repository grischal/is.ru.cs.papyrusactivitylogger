/**
 */
package task;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Modelling Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link task.ModellingTask#getModelURI <em>Model URI</em>}</li>
 * </ul>
 *
 * @see task.TaskPackage#getModellingTask()
 * @model
 * @generated
 */
public interface ModellingTask extends Task {
	/**
	 * Returns the value of the '<em><b>Model URI</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model URI</em>' attribute.
	 * @see #setModelURI(String)
	 * @see task.TaskPackage#getModellingTask_ModelURI()
	 * @model default=""
	 * @generated
	 */
	String getModelURI();

	/**
	 * Sets the value of the '{@link task.ModellingTask#getModelURI <em>Model URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model URI</em>' attribute.
	 * @see #getModelURI()
	 * @generated
	 */
	void setModelURI(String value);

} // ModellingTask
