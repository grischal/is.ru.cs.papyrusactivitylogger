/**
 */
package task.impl;

import org.eclipse.emf.ecore.EClass;

import task.BreakTask;
import task.TaskPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Break Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BreakTaskImpl extends TaskImpl implements BreakTask {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BreakTaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TaskPackage.Literals.BREAK_TASK;
	}

} //BreakTaskImpl
