/**
 */
package task;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>URL Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link task.URLTask#getURL <em>URL</em>}</li>
 * </ul>
 *
 * @see task.TaskPackage#getURLTask()
 * @model
 * @generated
 */
public interface URLTask extends Task {
	/**
	 * Returns the value of the '<em><b>URL</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>URL</em>' attribute list.
	 * @see task.TaskPackage#getURLTask_URL()
	 * @model required="true"
	 * @generated
	 */
	EList<String> getURL();

} // URLTask
