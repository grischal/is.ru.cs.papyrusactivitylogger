/**
 */
package task;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Break Task</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see task.TaskPackage#getBreakTask()
 * @model
 * @generated
 */
public interface BreakTask extends Task {
} // BreakTask
