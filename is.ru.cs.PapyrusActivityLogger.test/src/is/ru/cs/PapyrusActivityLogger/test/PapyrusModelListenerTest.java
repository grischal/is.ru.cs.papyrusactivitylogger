package is.ru.cs.PapyrusActivityLogger.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EStructuralFeatureImpl;
import org.eclipse.emf.transaction.ResourceSetChangeEvent;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.junit.framework.classification.tests.AbstractPapyrusTest;
import org.eclipse.papyrus.junit.utils.rules.PapyrusEditorFixture;
import org.eclipse.papyrus.junit.utils.rules.PluginResource;
import org.eclipse.uml2.uml.Model;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import is.ru.cs.papyrusActivityLogger.PapyrusModelModificationListener;
import is.ru.cs.papyrusActivityLogger.UserAction;

@PluginResource("resources/myTestModel.uml")
public class PapyrusModelListenerTest extends AbstractPapyrusTest {
	
	@Rule
	public final PapyrusEditorFixture editorSet = new PapyrusEditorFixture();
	
	public PapyrusModelListenerTest() {
		super();
	}
	
	@Before
	public void init() {
		final Model model = (Model) editorSet.getModel();
		Assert.assertNotNull("Model is null", model);
	}
	
	@Test
	/*
	 * Tests that resourceSetChanged actually looks into the different notifications and identifies them correctly.
	 */
	public void testBasicNotificationCapabilities() throws Exception {
		TransactionalEditingDomain editingDomain = editorSet.getEditingDomain();

		//The actual object for testing
		PapyrusModelModificationListener modelListener = new PapyrusModelModificationListener();
		
		//Adding a basic, empty ADD notification
		ArrayList<Notification> notifications = new ArrayList<Notification>(); 
		notifications.add(new ENotificationImpl(null,Notification.ADD, null, false, true));
		
		ResourceSetChangeEvent argument = new ResourceSetChangeEvent(editingDomain, null, notifications);
		modelListener.resourceSetChanged(argument);
	}
	
	@Test
	/**
	 * Tests that two actions >3 sec apart are correctly detected.
	 * @throws Exception
	 */
	public void testBasicHumanActions() throws Exception {
		TransactionalEditingDomain editingDomain = editorSet.getEditingDomain();

		PapyrusModelModificationListener modelListener = new PapyrusModelModificationListener();
		ArrayList<Notification> fakeNotifications = new ArrayList<Notification>();
	
		fakeNotifications.add(new ENotificationImpl(null,Notification.SET, new EStructuralFeatureImpl () {
			
		}, false, true));
		fakeNotifications.add(new ENotificationImpl(null,Notification.ADD, new EStructuralFeatureImpl () {
			
		}, null, EClass.class));
		ResourceSetChangeEvent argument = new ResourceSetChangeEvent(editingDomain, null, fakeNotifications);
		modelListener.resourceSetChanged(argument);
	
		List<UserAction> actionList = modelListener.getUserActions();
		assertEquals(actionList.size(),1);
		assertEquals(actionList.get(0).getNotifications().size(), 2);	
		assertEquals(actionList.get(0).getNotifications().get(0).feature, null);
		assertEquals(actionList.get(0).getNotifications().get(0).method, "SET");
		assertEquals(actionList.get(0).getNotifications().get(0).newValue, "(true, Boolean)");
		assertEquals(actionList.get(0).getNotifications().get(0).oldValue, "(false, Boolean)");
		assertEquals(actionList.get(0).getNotifications().get(1).feature, null);
		assertEquals(actionList.get(0).getNotifications().get(1).method, "ADD");
		assertEquals(actionList.get(0).getNotifications().get(1).newValue, "(" + EClass.class.toString() + ", Class)");
		assertEquals(actionList.get(0).getNotifications().get(1).oldValue, "(null, null)");
		
		//To cause two "human actions", we'll have to introduce timeouts.
		//Waiting for 3 secs (according to the task timeout in Umple).
		Thread.sleep(3001);
		
		fakeNotifications = new ArrayList<Notification>();
		fakeNotifications.add(new ENotificationImpl(null,Notification.REMOVE, new EStructuralFeatureImpl () {
			
		}, EClass.class, null));
		fakeNotifications.add(new ENotificationImpl(null,Notification.UNSET, new EStructuralFeatureImpl () {
			
		}, true, false));
		argument = new ResourceSetChangeEvent(editingDomain, null, fakeNotifications);
		modelListener.resourceSetChanged(argument);
		
		actionList = modelListener.getUserActions();
		assertEquals(actionList.size(),2);
		assertEquals(actionList.get(1).getNotifications().size(), 2);	
		assertEquals(actionList.get(1).getNotifications().get(0).feature, null);
		assertEquals(actionList.get(1).getNotifications().get(0).method, "REMOVE");
		assertEquals(actionList.get(1).getNotifications().get(0).newValue, "(null, null)");
		assertEquals(actionList.get(1).getNotifications().get(0).oldValue, "(" + EClass.class.toString() + ", Class)");
		assertEquals(actionList.get(1).getNotifications().get(1).feature, null);
		assertEquals(actionList.get(1).getNotifications().get(1).method, "OTHER: 2");
		assertEquals(actionList.get(1).getNotifications().get(1).newValue, "(false, Boolean)");
		assertEquals(actionList.get(1).getNotifications().get(1).oldValue, "(true, Boolean)");
	}
	
}
