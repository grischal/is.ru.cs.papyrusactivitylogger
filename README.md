# ModRec

## is.ru.cs.PapyrusActivityLogger
Contains various thing all related to the logging and tasks.
* The logger - ResourceSetListener for EMF based applications.
* Task import wizard - To import the task.json file.
* Task management
    * The Tasks - ModellingTask, BreakTask and URLTask.
    * The Task manager - Oversees task transition.
* Task view - Displays task information to the user.
* Logger starting button - Initilizes the logger on the current papyrus model.

## is.ru.cs.PapyrusActivityLogger.test
Tests for the logger.

## is.ru.cs.PapyrusActivityLogger.taskCreator
Tasks used in the task creation wizard.

## is.ru.cs.PapyrusActivityLogger.taskCreator.acceleo
Code generation for the task creation wizard.

## is.ru.cs.PapyrusActivityLogger.taskCreator.acceleo.ui
Code generation for the task creation wizard, UI part.

## is.ru.cs.PapyrusActivityLogger.updateSite.feature
Feature containing the *is.ru.cs.PapyrusActivityLogger* along with its dependencies.

## is.ru.cs.PapyrusActivityLogger.updateSite.update
The update site that contains the *is.ru.cs.PapyrusActivityLogger.updateSite.feature* feature.
