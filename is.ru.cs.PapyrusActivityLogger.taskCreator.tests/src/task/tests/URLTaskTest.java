/**
 */
package task.tests;

import junit.textui.TestRunner;

import task.TaskFactory;
import task.URLTask;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>URL Task</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class URLTaskTest extends TaskTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(URLTaskTest.class);
	}

	/**
	 * Constructs a new URL Task test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public URLTaskTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this URL Task test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected URLTask getFixture() {
		return (URLTask)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(TaskFactory.eINSTANCE.createURLTask());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //URLTaskTest
